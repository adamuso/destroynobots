﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DestroyNobots.Assembler.Parser
{
    public enum AssemblerParameters
    {
        REGISTER = 0x1,
        VALUE = 0x2,
        POINTER = 0x4
        //0x8
        //0x10
        //0x20 ...
    }
}
