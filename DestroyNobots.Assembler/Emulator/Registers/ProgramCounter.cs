﻿using System;

namespace DestroyNobots.Assembler.Emulator.Registers
{
    public class ProgramCounter<T>
        where T : struct, IConvertible
    {
        private Processor<T> processor;
        private Register<T> register;

        public ProgramCounter(Processor<T> processor, Register<T> register)
        {
            this.processor = processor;
            this.register = register;
        }

        public void Set(int address)
        {
            register.Value = (T)Convert.ChangeType(address, typeof(T));
        }

        public void Set(uint address)
        {
            register.Value = (T)Convert.ChangeType(address, typeof(T));
        }

        public void Branch(int address)
        {
            register.Value = (T)Convert.ChangeType(register.Value.ToUInt32(null) + address, typeof(T));
        }

        public void Jump(Pointer address)
        {
            register.Value = (T)Convert.ChangeType(address, typeof(T));
        }

        public void Call(Pointer address)
        {
            processor.StackPointer.Push(register.Value);
            register.Value = (T)Convert.ChangeType(address, typeof(T));
        }

        public void Return()
        {
            register.Value = processor.StackPointer.Pop();
        }

        public uint Address { get { return register.Value.ToUInt32(null); } }
    }
}
