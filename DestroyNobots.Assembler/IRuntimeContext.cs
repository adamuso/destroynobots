﻿namespace DestroyNobots.Assembler
{
    public interface IRuntimeContext
    {
        T GetContext<T>();
    }
}
