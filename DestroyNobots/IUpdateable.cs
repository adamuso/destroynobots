﻿using Microsoft.Xna.Framework;

namespace DestroyNobots
{
    interface IUpdateable
    {
        void Update(GameTime gt);
    }
}
